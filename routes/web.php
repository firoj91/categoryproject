<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (){
    return view('/welcome');
});

Route::get('/login', function () {
    return view('admin.login');
})->name('login');

Route::get('/register', function () {
    return view('admin.register');
});
//Auth::routes();

Route::get('/', 'HomeController@index')->name('welcome');

Route::group(['prefix'=>'admin', 'middleware'=>'auth', 'namespace'=>'admin'], function (){
   Route::get('dashboard','DashboardController@index')->name('dashboard');

   Route::resource('users','UserController');
//  Route::get('users/{id}/edit','UserController@edit');
//   Route::put('users/{id}','UserController@update');
//   Route::delete('users/{id}','UserController@destroy')->name('user.destroy');

    Route::resource('categories','CategoryController');

    Route::resource('logo','LogoController');
    Route::post('active/{id}','LogoController@active')->name('logo.active');
    Route::post('logo/{id}','LogoController@unactive')->name('logo.unactive');

    Route::resource('sliders','SliderController');

    Route::resource('services', 'ServiceController');

    Route::resource('portfolio', 'PortfolioController');

    Route::resource('team','TeamController');

});

Route::post('register', 'RegisterController@store')->name('register');
Route::post('login', 'SessionController@login');
Route::get('logout', 'SessionController@logout');