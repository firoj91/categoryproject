<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = ['title','description','image','category_id'];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
