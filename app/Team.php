<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = ['name','image','position','facebook','twitter','google','linkedin'];
}
