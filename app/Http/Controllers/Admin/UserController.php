<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;

class UserController extends Controller
{
    const UPLOAD_DIR = '/uploads/users/';

    public function index()
    {
        $users = User::all();
        return view('admin.users.index', compact('users'));
    }

    public function show($id)
    {
      $users = User::all();
      return view('admin.users.show', compact('users'));
    }

    public function edit(User $user)
    {

        return view('admin.users.edit', compact('user'));
    }

    public function update(UserRequest $request, User $user)
    {

        $data = $request->only('name','email','image','role');

        if ($request->hasFile('image')) {
            $data['image'] = $this->upload($request->image);
        }

        $this->unlink($user->image);
        $user->update($data);;

        return redirect('admin/users');
    }


    public function destroy($id)
    {
        $user = User::all();
        $user->delete();

        return redirect('admin/categories');
    }



    private function upload($file, $title = ''){
        $timestamp = str_replace([' ',':'], '-', Carbon::now()->toDateTimeString());
        $file_name = $timestamp . '-'.$title . '.' .$file->getClientOriginalExtension();
        Image::make($file)->resize(300,300)->save(public_path() . self::UPLOAD_DIR . $file_name);
        return $file_name;
    }

    private function unlink($file)
    {
        if ($file != '' && file_exists(public_path() . self::UPLOAD_DIR . $file)){
            @unlink(public_path(). self::UPLOAD_DIR . $file);
        }
    }

}
