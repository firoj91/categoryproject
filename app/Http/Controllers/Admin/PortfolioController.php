<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PortfolioRequest;
use App\Portfolio;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;

class PortfolioController extends Controller
{
    const UPLOAD_DIR = '/uploads/portfolio/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolios = Portfolio::all();
        return view('admin.portfolio.index', compact('portfolios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.portfolio.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PortfolioRequest $request)
    {
        $data = $request->only('url','image');
        if ($request->hasFile('image'))
        {
          $data['image'] = $this->upload($request->image) ;
        }

        Portfolio::create($data);
        Toastr::success('Portfolio added successfully','Success',['positionClass'=>'toast-top-right']);
        return redirect('admin/portfolio');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Portfolio $portfolio)
    {
        return view('admin.portfolio.edit', compact('portfolio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PortfolioRequest $request, Portfolio $portfolio)
    {
        $data = $request->only('url','image');
        if ($request->hasFile('image'))
        {
            $data['image'] = $this->upload($request->image) ;
        }
        $this->unlink($portfolio->image);
        $portfolio->update($data);

        Toastr::success('Portfolio Updated successfully','Success',['positionClass'=>'toast-top-right']);
        return redirect('admin/portfolio');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Portfolio $portfolio)
    {
        $this->unlink($portfolio->image);
        $portfolio->delete();
        Toastr::success('Portfolio Deleted successfuly','Success',['positionClass'=>'toast-top-right']);
    }

    private function upload($file, $title = '')
    {
        $timestamp = str_replace(['',':'],'-', Carbon::now()->toDateTimeString());
        $file_name = $timestamp . '-'.$title. $file->getClientOriginalExtension();
        Image::make($file)->save(public_path(). self::UPLOAD_DIR . $file_name);
        return $file_name;
    }

    private function unlink($file)
    {
        if ($file != '' && file_exists(public_path() . self::UPLOAD_DIR . $file)){
            @unlink(public_path(). self::UPLOAD_DIR . $file);
        }

        return redirect('admin/portfolio');
    }
}
