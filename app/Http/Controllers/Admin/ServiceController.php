<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests\ServiceRequest;
use App\Service;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;

class ServiceController extends Controller
{
    const UPLOAD_DIR = '/uploads/service/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();
        return view('admin.services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::pluck('name','id');
        return view('admin.services.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceRequest $request)
    {
        $data = $request->only('title','description','image','category_id');

        if ($request->hasFile('image')){
            $data['image'] = $this->upload($request->image);
        }

        Service::create($data);
        Toastr::success('Services added successfully','Success',['positionClass'=>'toast-top-right']);
        return redirect('admin/services');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        return view('admin.services.show', compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        $category = Category::pluck('name','id');
        return view('admin.services.edit', compact('service','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceRequest $request, Service $service)
    {
        $data = $request->only('title','description','image','category_id');

        if ($request->hasFile('image')){
            $data['image'] = $this->upload($request->image);
        }
        $this->unlink($service->image);
        $service->update($data);
        Toastr::success('Services update successfully','Success',['positionClass'=>'toast-top-right']);
        return redirect('admin/services');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $this->unlink($service->image);
        $service->delete();
        Toastr::success('Services deleted successfully','Success',['positionClass'=>'toast-top-right']);
        return redirect('admin/services');
    }

    private function upload($file, $title = '')
    {
        $timestamp = str_replace([' ',':'],'-', Carbon::now()->toDateTimeString());
        $file_name = $timestamp . '-' . $title .'.' . $file->getClientOriginalExtension();
        Image::make($file)->save(public_path() . self::UPLOAD_DIR . $file_name);
        return $file_name;
    }

    public function unlink($file)
    {
        if ($file != '' && file_exists(public_path() . self::UPLOAD_DIR . $file)){
            @unlink(public_path() . self::UPLOAD_DIR . $file);
        }

        return redirect('admin/services');
    }
}
