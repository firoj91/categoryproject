<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LogoRequest;
use App\Logo;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Intervention\Image\Facades\Image;

class LogoController extends Controller
{
    const UPLOAD_DIR = '/uploads/logo/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logos = Logo::all();
        return view('admin.logo.index', compact('logos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.logo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LogoRequest $request)
    {
        $data = $request->only('image','status');
        if ($request->hasFile('image')) {

            $data['image'] = $this->upload($request->image);
        }

        Logo::create($data);
        Toastr::success('Logo Added successfully','Success',["positionClass"=>"toast-top-right"]);
        return redirect('admin/logo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Logo $logo)
    {
        return view('admin.logo.edit',compact('logo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LogoRequest $request, Logo $logo)
    {
        $data = $request->only('image','status');

        if ($request->hasFile('image')){
            $data['image'] = $this->upload($request->image);
        }
        $this->unlink($logo->image);


        $logo->update($data);
        Toastr::success('Logo Added successfully','Success',["positionClass"=>"toast-top-right"]);
        return redirect('admin/logo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Logo $logo)
    {

        $this->unlink($logo->image);
        $logo->delete();


        Toastr::success('Logo Added successfully','Success',["positionClass"=>"toast-top-right"]);
        return redirect('admin/logo');
    }

    private function upload($file, $title = ''){
        $timestamp = str_replace([' ',':'], '-', Carbon::now()->toDateTimeString());
        $file_name = $timestamp . '-'.$title . '.' .$file->getClientOriginalExtension();
        Image::make($file)->save(public_path() . self::UPLOAD_DIR . $file_name);
        return $file_name;
    }

    private function unlink($file)
    {
        if ($file != '' && file_exists(public_path() . self::UPLOAD_DIR . $file)){
            @unlink(public_path() . self::UPLOAD_DIR . $file);
        }
        return redirect('admin/logo');
    }

    public function active($id)
    {
        $logo = Logo::find($id);
        $logo->status = true;
        $logo->save();
        Toastr::success('Logo active successfully','success',["positionClass"=>"toast-top-right"]);
        return redirect()->back();
    }

    public function unactive($id)
    {
        $logo = Logo::find($id);
        $logo->status = false;
        $logo->save();
        Toastr::success('Logo active successfully','success',["positionClass"=>"toast-top-right"]);
        return redirect()->back();
    }
}
