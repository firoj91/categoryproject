<?php

namespace App\Http\Controllers;

use App\Category;
use App\Logo;
use App\Portfolio;
use App\Service;
use App\Slider;
use App\Team;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::all();
        $categories = Category::all();
        $logos = Logo::where('status',true)->get();
        $sliders = Slider::all();
        $services = Service::all();
        $portfolios = Portfolio::all();
        return view('welcome', compact('logos','sliders','categories','services','portfolios','teams'));
    }
}
