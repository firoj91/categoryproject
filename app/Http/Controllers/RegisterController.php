<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;

class RegisterController extends Controller
{
    const UPLOAD_DIR = '/uploads/users/';
    public function create()
    {
        return view('admin.register');
    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|min:5',
            'email' => 'required|email',
            'password' => 'required|min:6|confirmed',
            'image' => 'required|mimes:jpeg,bmp,png',

        ]);
        $validatedData['password']= bcrypt($validatedData['password']);

        if ($request->hasFile('image')) {
            $validatedData['image'] = $this->upload($request->image);
        }
        $user = User::create($validatedData);
        auth()->login($user);

        return redirect('admin/dashboard');
    }

    private function upload($file, $title = ''){
        $timestamp = str_replace([' ',':'], '-', Carbon::now()->toDateTimeString());
        $file_name = $timestamp . '-'.$title . '.' .$file->getClientOriginalExtension();
        Image::make($file)->resize(300,300)->save(public_path() . self::UPLOAD_DIR . $file_name);
        return $file_name;
    }
}
