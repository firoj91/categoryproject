<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class SessionController extends Controller
{
    public function login(Request $request)
    {
        if (!auth()->attempt(request(['email','password']))){
            return back()->withErrors([
                'message' => 'Please check your credentials'
            ]);
        }
        return redirect('admin/dashboard');

    }

    public function logout()
    {
        auth()->logout();
        return redirect('/');
    }

}
