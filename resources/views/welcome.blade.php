<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>csfworld- Best Web Design and Development company</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    {!! Html::style('frontend/lib/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('frontend/lib/font-awesome/css/font-awesome.min.css') !!}
    {!! Html::style('frontend/lib/animate/animate.min.css') !!}
    {!! Html::style('frontend/lib/ionicons/css/ionicons.min.css') !!}
    {!! Html::style('frontend/lib/owlcarousel/assets/owl.carousel.min.css') !!}
    {!! Html::style('frontend/lib/lightbox/css/lightbox.min.css') !!}
    {!! Html::style('frontend/css/style.css') !!}

    <link href="img/favicon.png" rel="icon">
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon">


    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">


</head>

<body>

<header id="header">
    <div class="container-fluid">
        @foreach($logos as $logo)
            <div id="logo" class="pull-left">
                <img src="{{asset('uploads/logo/'.$logo->image)}}" style="heigh:100px; width:100px; border-radius:50%;" alt="" >
            </div>
        @endforeach
        <nav id="nav-menu-container">
            <ul class="nav-menu">
                <li class="menu-active"><a href="#intro">Home</a></li>
                <li><a href="#about">About Us</a></li>
                <li><a href="#services">Services</a></li>
                <li><a href="#portfolio">Portfolio</a></li>
                <li><a href="#team">Team</a></li>
                <li><a href="#contact">Contact</a></li>
            </ul>
        </nav>
    </div>
</header>

<section id="intro">
    <div class="intro-container">
        <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

            <ol class="carousel-indicators">
                @foreach($sliders as $slider)
                    <li data-target="#introCarousel" data-slide-to="{{$loop->index}}" class="{{$loop->first? 'active':''}}"></li>
                @endforeach
            </ol>

            <div class="carousel-inner" role="listbox">
                @foreach($sliders as $slider)
                    <div class="carousel-item {{$loop->first? 'active':''}}">
                        <div class="carousel-background"><img src="{{asset('uploads/slider/'.$slider->image)}}" alt=""></div>
                        <div class="carousel-container">
                            <div class="carousel-content">
                                <h2>{{$slider->title}}</h2>
                                <p>{{$slider->sub_title}}</p>
                            </div>
                        </div>
                    </div>

              @endforeach

            </div>

            <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

        </div>
    </div>
</section>

<main id="main">

    <section id="featured-services">
        <div class="container">
            <div class="row">

                <div class="col-lg-4 box">
                    <i class="ion-ios-bookmarks-outline"></i>
                    <h4 class="title"><a href="">Lorem Ipsum Delino</a></h4>
                    <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
                </div>

                <div class="col-lg-4 box box-bg">
                    <i class="ion-ios-stopwatch-outline"></i>
                    <h4 class="title"><a href="">Dolor Sitema</a></h4>
                    <p class="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
                </div>

                <div class="col-lg-4 box">
                    <i class="ion-ios-heart-outline"></i>
                    <h4 class="title"><a href="">Sed ut perspiciatis</a></h4>
                    <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>
                </div>

            </div>
        </div>
    </section>


    <section id="about">
        <div class="container">

            <header class="section-header">
                <h3>About Us</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            </header>

            <div class="row about-cols">

                <div class="col-md-4 wow fadeInUp">
                    <div class="about-col">
                        <div class="img">
                            <img src="img/about-mission.jpg" alt="" class="img-fluid">
                            <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
                        </div>
                        <h2 class="title"><a href="#">Our Mission</a></h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                </div>

                <div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="about-col">
                        <div class="img">
                            <img src="img/about-plan.jpg" alt="" class="img-fluid">
                            <div class="icon"><i class="ion-ios-list-outline"></i></div>
                        </div>
                        <h2 class="title"><a href="#">Our Plan</a></h2>
                        <p>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                        </p>
                    </div>
                </div>

                <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="about-col">
                        <div class="img">
                            <img src="img/about-vision.jpg" alt="" class="img-fluid">
                            <div class="icon"><i class="ion-ios-eye-outline"></i></div>
                        </div>
                        <h2 class="title"><a href="#">Our Vision</a></h2>
                        <p>
                            Nemo enim ipsam voluptatem quia voluptas sit aut odit aut fugit, sed quia magni dolores eos qui ratione voluptatem sequi nesciunt Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.
                        </p>
                    </div>
                </div>

            </div>

        </div>
    </section>

    <section id="services">
        <div class="container">

            <header class="section-header wow fadeInUp">
                <h3>Services</h3>
            </header>
            <div class="row">
                <div class="col-lg-12">
                    <ul id="services-flters">
                        <li data-filter="*" class="filter-active">All</li>
                         @foreach($categories as $category)
                            <li class="filter" data-filter="#{{$category->slug}}">{{$category->name}}</li>
                         @endforeach
                    </ul>
                </div>
            </div>

            <div class="row">
                @foreach($services as $service)
                    <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">

                        <h4 class="title" id="{{$service->category->slug}}"><a href="">{{$service->title}}</a></h4>
                        <img src="{{asset('uploads/service/'.$service->image)}}"  class="img-fluid" alt="">
                        <p style="margin-top: 20px;">{{$service->description}}</p>
                    </div>
                @endforeach
            </div>
        </div>
    </section>




    <section id="portfolio"  class="section-bg" >
        <div class="container">

            <header class="section-header">
                <h3 class="section-title">Our Portfolio</h3>
            </header>

            {{--<div class="row">--}}
                {{--<div class="col-lg-12">--}}
                    {{--<ul id="portfolio-flters">--}}
                        {{--<li data-filter="*" class="filter-active">All</li>--}}

                        {{--@foreach($categories as $category)--}}
                        {{--<li data-filter="#{{$category->slug}}">{{$category->name}}</li>--}}
                        {{--@endforeach--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="row portfolio-container">

                @foreach($portfolios as $portfolio)
                <div class="col-lg-4 col-md-6 portfolio-item filter-app wow fadeInUp">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="{{asset('uploads/portfolio/'.$portfolio->image)}}" class="img-fluid" alt="">
                            <a href="{{asset('uploads/portfolio/'.$portfolio->image)}}" data-lightbox="portfolio" data-title="App 1" class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="{{$portfolio->url}}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                        </figure>
                    </div>
                </div>
                @endforeach


            </div>

        </div>
    </section>


    {{--<section id="clients" class="wow fadeInUp">--}}
        {{--<div class="container">--}}

            {{--<header class="section-header">--}}
                {{--<h3>Our Clients</h3>--}}
            {{--</header>--}}

            {{--<div class="owl-carousel clients-carousel">--}}
                {{--<img src="img/clients/client-1.png" alt="">--}}
                {{--<img src="img/clients/client-2.png" alt="">--}}
                {{--<img src="img/clients/client-3.png" alt="">--}}
                {{--<img src="img/clients/client-4.png" alt="">--}}
                {{--<img src="img/clients/client-5.png" alt="">--}}
                {{--<img src="img/clients/client-6.png" alt="">--}}
                {{--<img src="img/clients/client-7.png" alt="">--}}
                {{--<img src="img/clients/client-8.png" alt="">--}}
            {{--</div>--}}

        {{--</div>--}}
    {{--</section>--}}

    <section id="team">
        <div class="container">
            <div class="section-header wow fadeInUp">
                <h3>Team</h3>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
            </div>

            <div class="row">
                @foreach($teams as $team)
                    <div class="col-lg-3 col-md-6 wow fadeInUp">
                        <div class="member">
                            <img src="{{asset('uploads/team/'.$team->image)}}" class="img-fluid" alt="">
                            <div class="member-info">
                                <div class="member-info-content">
                                    <h4>{{$team->name}}</h4>
                                    <span>{{$team->position}}</span>
                                    <div class="social">
                                        <a href="{{$team->twitter}}"><i class="fa fa-twitter"></i></a>
                                        <a href="{{$team->facebook}}"><i class="fa fa-facebook"></i></a>
                                        <a href="{{$team->google}}"><i class="fa fa-google-plus"></i></a>
                                        <a href="{{$team->linkedin}}"><i class="fa fa-linkedin"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </section>

    <section id="contact" class="section-bg wow fadeInUp">
        <div class="container">

            <div class="section-header">
                <h3>Contact Us</h3>

            </div>

            <div class="row contact-info">

                <div class="col-md-4">
                    <div class="contact-address">
                        <a href=""><i class="ion-ios-location-outline"></i></a>
                        <h3>Address</h3>
                        <address>A108 Adam Street, NY 535022, USA</address>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="contact-phone">
                        <i class="ion-ios-telephone-outline"></i>
                        <h3>Phone Number</h3>
                        <p><a href="tel:+155895548855">+1 5589 55488 55</a></p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="contact-email">
                        <i class="ion-ios-email-outline"></i>
                        <h3>Email</h3>
                        <p><a href="mailto:info@example.com">info@example.com</a></p>
                    </div>
                </div>

            </div>

            <div class="form">
                <div id="sendmessage">Your message has been sent. Thank you!</div>
                <div id="errormessage"></div>
                <form action="" method="post" role="form" class="contactForm">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                            <div class="validation"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                            <div class="validation"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                        <div class="validation"></div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                        <div class="validation"></div>
                    </div>
                    <div class="text-center"><button type="submit">Send Message</button></div>
                </form>
            </div>

        </div>
    </section>

</main>
<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-4 col-md-4 footer-info">
                    <h3>Head Office</h3>
                    <p>
                        House 420, Road 34, Mirpur 10 </br>
                        Dhaka 1210</br>
                        Dhaka Bangladesh
                    </p>
                    <br>
                    <h2> <strong>Ranpur Brance</strong></h2>
                    <p>

                        House 112,
                        Rangpur Sadar</br>
                        Rangpur Bangladesh
                    </p>
                </div>

                <div class="col-lg-4 col-md-4 footer-links">
                    <h4>Useful Links</h4>
                    <ul>
                        <li><i class="ion-ios-arrow-right"></i> <a href="#intro">Home</a></li>
                        <li><i class="ion-ios-arrow-right"></i> <a href="#about">About us</a></li>
                        <li><i class="ion-ios-arrow-right"></i><a href="#services">Services</a></li>
                        <li><i class="ion-ios-arrow-right"></i> <a href="#">Terms of service</a></li>
                        <li><i class="ion-ios-arrow-right"></i> <a href="#">Privacy policy</a></li>
                    </ul>
                </div>

                <div class="col-lg-4 col-md-4 footer-contact">
                    <h4>Contact Us</h4>
                    <p>
                        Mirpur 10 <br>
                       Dhaka Bangladesh<br>

                        <strong>Phone:</strong> 01773232923<br>
                        <strong>Email:</strong> info@csfworld.com<br>
                        <strong>Skype:</strong>csfworld<br>
                    </p>

                    <div class="social-links">
                        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                        <a href="https://www.facebook.com/" class="facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                        <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                    </div>

                </div>



            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright">
            <img src="{{asset('frontend/img/about-mission.jpg')}}" alt="" style="height: 50px; width:100px;">
            All Rights Reserved &copy; Copyright <strong>csfworld</strong>.
        </div>

    </div>
</footer>

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
{!! Html::script('frontend/lib/jquery/jquery.min.js') !!}
{!! Html::script('frontend/lib/jquery/jquery-migrate.min.js') !!}
{!! Html::script('frontend/lib/bootstrap/js/bootstrap.bundle.min.js') !!}
{!! Html::script('frontend/lib/easing/easing.min.js') !!}
{!! Html::script('frontend/lib/superfish/hoverIntent.js') !!}
{!! Html::script('frontend/lib/superfish/superfish.min.js') !!}
{!! Html::script('frontend/lib/wow/wow.min.js') !!}
{!! Html::script('frontend/lib/waypoints/waypoints.min.js') !!}
{!! Html::script('frontend/lib/counterup/counterup.min.js') !!}
{!! Html::script('frontend/lib/owlcarousel/owl.carousel.min.js') !!}
{!! Html::script('frontend/lib/isotope/isotope.pkgd.min.js') !!}
{!! Html::script('frontend/lib/lightbox/js/lightbox.min.js') !!}
{!! Html::script('frontend/lib/touchSwipe/jquery.touchSwipe.min.js') !!}
{!! Html::script('frontend/contactform/contactform.js') !!}
{!! Html::script('frontend/js/main.js') !!}



</body>
</html>
