@extends('admin.layouts.master')
@section('title','slider')
@section('content')
    <div class="col-md-12">
        <div class="card">

            <div class="card-content">
                <div class="row">
                    {!! Form::model( $slider, ['url' => ['admin/sliders', $slider->id], 'files'=> true,  'method' => 'put']) !!}

                    @include('admin.sliders.form')

                    <div class="form-group" style="margin-top:20px;">

                        {!! Form::submit('Update', ["type"=>"submit", "class" => "btn btn-primary"]) !!}

                        <a href="{{url('admin/sliders')}}" class="btn btn-info">Back</a>

                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection