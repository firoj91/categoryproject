<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SB Admin - Start Bootstrap Template</title>
    {!! Html::style('backend/bootstrap.min.css') !!}
    {!! Html::style('backend/css/font-awesome.min.css') !!}
    {!! Html::style('backend/css/sb-admin.css') !!}
</head>

<body class="bg-dark">
<div class="container">

    <div class="card card-register mx-auto mt-5">

        <div class="card-header">Register an Account</div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{route('register')}}" method ="post" enctype="multipart/form-data">

                @csrf
                <div class="form-group">
                    <label for="name">Username</label>
                    <input type="text" name="name" class="form-control" id="name" aria-describedby="name" placeholder="Enter Username..">
                </div>
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="text" name="email" class="form-control" id="email" aria-describedby="email" placeholder="Enter email..">
                </div>

                <div class="form-group">
                    <label for="email">Image</label>
                    <input type="file" name="image" id="image" aria-describedby="image" >
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="Password...">
                </div>
                <div class="form-group">
                    <label for="password">Confirm Password</label>
                    <input type="password" name="password_confirmation" class="form-control" id="password" placeholder="Confirm Password..">
                </div>
                <button type="submit" name="submit" class="btn btn-block btn-primary">Submit</button>
                <div class="form-group d-flex align-items-center justify-content-between">
                    <div class="icheck-square">
                        <input tabindex="1" type="checkbox" id="remember">
                        <label for="remember">Remember me</label>
                    </div>
                    <a href="#" class="forgot-pass">Forgot password</a>
                </div>
                <div class="d-flex justify-content-center mb-4">
                    <a href="https://www.facebook.com/" class="facebook-login btn btn-primary mr-2">Facebook</a>
                    <a href="https://plus.google.com/" class="google-login btn btn-danger">Google+</a>
                </div>
                <small class="text-center d-block mb-3">Already have an Account?<a href="{{url('/login')}}">login</a></small>
                <small class="text-center d-block">By creating an account you are accepting our<a href="#"> Terms & Conditions</a></small>
            </form>


        </div>
    </div>
    </div>

{!! Html::script('backend/js/jquery.min.js') !!}
{!! Html::script('backend/js/bootstrap.bundle.min.js') !!}
{!! Html::script('backend/js/jquery.easing.min.js') !!}
</body>

</html>
