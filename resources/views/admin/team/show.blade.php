@extends('admin.layouts.master')
@section('title','team')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row" style="padding: 5px">
                        <div class="pull-left">Team Member</div>
                        <div class="pull-right"><a class="btn btn-sm btn-primary" title="team member" href="{{url('admin/team')}}"><i class="fa fa-list"></i>Team</a></div>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <div class="row">
                    <p><strong>Name:- </strong> {{$team->name}}</p>
                    <p><strong>Position:- </strong> {{$team->position}}</p>
                    <p><strong>Facebook:- </strong> {{$team->facebook}}</p>
                    <p><strong>Google Plus:- </strong> {{$team->google}}</p>
                    <p><strong>Twitter:- </strong> {{$team->twitter}}</p>
                    <p><strong>Linkedin:- </strong> {{$team->linkedin}}</p>

                </div>
            </div>
        </div>
    </div>
@endsection