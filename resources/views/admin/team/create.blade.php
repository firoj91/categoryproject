@extends('admin.layouts.master')
@section('title','team')
@section('content')
    <div class="col-md-12">
        <div class="card">
            @include('admin.layouts.msg')
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row" style="padding: 5px">
                        <div class="pull-left">Team member</div>
                        <div class="pull-right"><a class="btn btn-sm btn-primary" title="team List" href="{{url('admin/team')}}"><i class="fa fa-list"></i>All member</a></div>
                    </div>
                </div>
            </div>
            <div class="card-content">

                <div class="row">
                    {!! Form::open(['url' => 'admin/team/','files'=>true]) !!}
                    @include('admin.team.form')

                    <div class="form-group" style="margin-top: 20px;">
                        {!! Form::submit('Add',["class"=>"btn btn-primary"]) !!}
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection