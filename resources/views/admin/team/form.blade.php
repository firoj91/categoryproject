<div class="row">
    <div class="col-md-12">
        <div class="form-group label-floating">
            {!! Form::label('name','Name',[ "class"=>"control-label"]) !!}
            {!! Form::text('name', null,["class"=>"form-control", 'autofocus']) !!}
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group label-floating">
            {!! Form::label('position','Position',[ "class"=>"control-label"]) !!}
            {!! Form::text('position', null,["class"=>"form-control", 'autofocus']) !!}
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group label-floating">
            {!! Form::label('facebook','Facebook',[ "class"=>"control-label"]) !!}
            {!! Form::text('facebook', null,["class"=>"form-control", 'autofocus']) !!}
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group label-floating">
            {!! Form::label('google','Google Plus',[ "class"=>"control-label"]) !!}
            {!! Form::text('google', null,["class"=>"form-control", 'autofocus']) !!}
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group label-floating">
            {!! Form::label('twitter','Twitter',[ "class"=>"control-label"]) !!}
            {!! Form::text('twitter', null,["class"=>"form-control", 'autofocus']) !!}
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group label-floating">
            {!! Form::label('linkedin','Linkedin',[ "class"=>"control-label"]) !!}
            {!! Form::text('linkedin', null,["class"=>"form-control", 'autofocus']) !!}
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label class="control-label">Image</label>
        <input type="file" name="image">
    </div>
</div>