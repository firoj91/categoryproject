@extends('admin.layouts.master')
@section('title','portfolio')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row" style="padding: 5px">
                        <div class="pull-left">Portfolio</div>
                        <div class="pull-right"><a class="btn btn-sm btn-primary" title="Post List" href="{{url('admin/portfolio')}}"><i class="fa fa-list"></i>Portfolio</a></div>
                    </div>
                </div>
            </div>
            @include('admin.layouts.msg')
            <div class="card-content">
                <div class="row">
                    {!! Form::model( $portfolio, ['url' => ['admin/portfolio', $portfolio->id], 'files'=> true,  'method' => 'put']) !!}

                    @include('admin.portfolio.form')

                    <div class="form-group" style="margin-top: 20px">

                        {!! Form::submit('Update', ["type"=>"submit", "class" => "btn btn-primary"]) !!}

                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection