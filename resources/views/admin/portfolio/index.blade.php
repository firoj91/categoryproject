@extends('admin.layouts.master')
@section('title','service')
@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
@endpush
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row" style="padding: 5px">
                        <div class="pull-left">Portfolio</div>
                        <div class="pull-right"><a class="btn btn-sm btn-primary" title="Portfolio List" href="{{url('admin/portfolio/create')}}"><i class="fa fa-plus"></i>Add New</a></div>
                    </div>
                </div>
            </div>
            <div class="card-content table-responsive">

                <table id="table" class="table table-striped table-bordered">
                    <thead class="text-primary">
                    <tr>
                        <th>Sl no</th>
                        <th>Url</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($portfolios as $portfolio)
                        <tr>
                            <td>{{$portfolio->id}}</td>
                            <td>{{$portfolio->url}}</td>
                            <td><img src="{{url('uploads/portfolio/',$portfolio->image)}}" style="border-radius:5px; height:80px; width:160px;" alt=""></td>

                            <td>
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a class="btn btn-sm btn-info" href="{{url( 'admin/portfolio/'.$portfolio->id.'/edit')}}" ><i class="fa fa-pencil"></i></a></li>
                                    <li class="list-inline-item">
                                        {!! Form::open(['url'=>['admin/portfolio/'.$portfolio->id],'method'=>'delete']) !!}
                                        {!! Form::button("<i class='fa fa-trash'></i>",[
                                        'type'=>'submit',
                                        'onClick'=>"return confirm('Are You sure Delete $portfolio->image ?')",
                                        'class'=>'btn btn-sm btn-danger'
                                        ]) !!}
                                        {!! Form::close() !!}
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script >
        $(document).ready(function() {
            $('#table').DataTable();
        } );
    </script>
@endpush