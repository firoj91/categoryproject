<div class="row">
    <div class="col-md-12">
        <div class="form-group label-floating">
            {!! Form::label('url','Url',[ "class"=>"control-label"]) !!}
            {!! Form::text('url', null,["class"=>"form-control", 'autofocus']) !!}
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label class="control-label">Image</label>
        <input type="file" name="image">
    </div>
</div>
