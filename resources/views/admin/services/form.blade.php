<div class="row">
    <div class="col-md-12">
        <div class="form-group label-floating">
            {!! Form::label('category_id','Category',[ "class"=>"control-label"]) !!}
            {!! Form::select('category_id', $category,null,["class"=>"form-control", 'autofocus', "placeholder"=>"Please select category"]) !!}
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group label-floating">
            {!! Form::label('title','Title',[ "class"=>"control-label"]) !!}
            {!! Form::text('title', null,["class"=>"form-control", 'autofocus']) !!}
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group label-floating">
            {!! Form::label('description','Description',[ "class"=>"control-label"]) !!}
            {!! Form::textarea('description', null,["class"=>"form-control", 'autofocus']) !!}
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label class="control-label">Image</label>
        <input type="file" name="image">
    </div>
</div>

@push('scripts')
    <script src="//cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('description', {
            uiColor: '#f1f1f1'
        });
    </script>
@endpush