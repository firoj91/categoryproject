@extends('admin.layouts.master')
@section('title','service')
@section('content')
    <div class="col-md-12">
        <div class="card">


            <div class="card-content">
                <div class="row">
                    {!! Form::model( $service, ['url' => ['admin/services', $service->id], 'files'=> true,  'method' => 'put']) !!}

                    @include('admin.services.form')

                    <div class="form-group" style="margin-top: 20px">

                        {!! Form::submit('Update', ["type"=>"submit", "class" => "btn btn-primary"]) !!}
                        <a href="{{url('admin/services')}}" title="Add service" class="btn btn-info">Back</a>

                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection  