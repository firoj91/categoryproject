@extends('admin.layouts.master')
@section('title','service')
@section('content')
    <div class="col-md-12">

        <div class="card">
            @include('admin.layouts.msg')
            <div class="card-header" data-background-color="purple">
                <h4 class="title">Services
                    <span class="pull-right"><a href="{{url('admin/services')}}" ><i class="material-icons">list</i>Back</a></span>
                </h4>
            </div>

            <div class="card-content">

                <div class="row">
                    {!! Form::open(['url' => 'admin/services/','files'=>true]) !!}
                    @include('admin.services.form')

                    <div class="form-group" style="margin-top: 20px;">
                        {!! Form::submit('Add',["class"=>"btn btn-primary"]) !!}
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection  