@extends('admin.layouts.master')
@section('title','service')
@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
@endpush
@section('content')
    <div class="col-md-12">
        <div class="card">
            <a href="{{url('admin/services')}}" class="btn btn-primary">Back</a>
            <p><strong>Category:- </strong>{{$service->category->name}}</p>
            <p><strong>Category:- </strong>{{$service->title}}</p>
            <p><strong>Category:- </strong>{{$service->description}}</p>
            <p><strong>Category:- </strong>{{$service->created_at}}</p>
            <p><strong>Category:- </strong>{{$service->updated_at}}</p>

        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script >
        $(document).ready(function() {
            $('#table').DataTable();
        } );
    </script>
@endpush