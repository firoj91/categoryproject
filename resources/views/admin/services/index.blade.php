@extends('admin.layouts.master')
@section('title','service')
@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
@endpush
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header " data-background-color="purple" >
                <h4 class="title">Services
                    <span class="pull-right"><a href="{{url('admin/services/create')}}" title="Add Service"><i class="material-icons">note_add</i>Add New</a></span>
                </h4>

            </div>
            <div class="card-content table-responsive">

                <table id="table" class="table table-striped table-bordered">
                    <thead class="text-primary">
                    <tr>
                        <th>Sl no</th>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($services as $service)
                        <tr>
                            <td>{{$service->id}}</td>
                            <td>{{$service->category->name}}</td>
                            <td>{{$service->title}}</td>
                            <td><img src="{{url('uploads/service/',$service->image)}}" style="border-radius:5px; height:80px; width:160px;" alt=""></td>

                            <td>
                                <ul class="list-inline">

                                    <li class="list-inline-item"><a class="btn btn-sm btn-primary" href="{{url( 'admin/services/'.$service->id)}}" ><i class="fa fa-eye"></i></a></li>
                                    <li class="list-inline-item"><a class="btn btn-sm btn-info" href="{{url( 'admin/services/'.$service->id.'/edit')}}" ><i class="fa fa-pencil"></i></a></li>
                                    <li class="list-inline-item">
                                        {!! Form::open(['url'=>['admin/services/'.$service->id],'method'=>'delete']) !!}
                                        {!! Form::button("<i class='fa fa-trash'></i>",[
                                        'type'=>'submit',
                                        'onClick'=>"return confirm('Are You sure Delete $service->slider ?')",
                                        'class'=>'btn btn-sm btn-danger'
                                        ]) !!}
                                        {!! Form::close() !!}
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script >
        $(document).ready(function() {
            $('#table').DataTable();
        } );
    </script>
@endpush