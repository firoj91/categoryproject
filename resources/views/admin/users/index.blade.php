@extends('admin.layouts.master')
@section('title','List')
@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
@endpush
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-content table-responsive">

                <table id="table" class="table table-striped table-bordered">
                    <thead class="text-primary">
                    <tr>
                        <th>Sl no</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>
                                @if($user->role == 1)
                                    {{'Admin'}}
                                    @elseif($user->role == 2)
                                    {{'Editor'}}
                                    @else
                                    {{'Gest'}}
                                @endif


                            </td>
                            <td>{{$user->image}}</td>
                            <td>
                                <a class="btn btn-sm btn-info" href="{{url( 'admin/users/'.$user->id.'/edit')}}" ><i class="material-icons">mode_edit</i></a>

                                <form action="{{url('admin/users/',$user->id)}}" id="delete-form-{{$user->id}}" style="display: none;" method="post">
                                    @csrf
                                    @method('DELETE')
                                </form>
                                <button type="button" class="btn btn-sm btn-danger" onclick="if(confirm('Are you sure to Delete?')){
                                        event.preventDefault();
                                        document.getElementById('delete-form-{{$user->id}}').submit();
                                        }else {
                                        event.preventDefault();
                                        }">
                                    <i class="material-icons">delete</i>

                                </button>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script >
        $(document).ready(function() {
            $('#table').DataTable();
        } );
    </script>
@endpush