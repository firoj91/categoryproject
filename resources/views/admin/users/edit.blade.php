@extends('admin.layouts.master')
@section('content')
    <div class="col-md-12">
        <div class="card">

                {!! Form::open(['url' => ['admin/users',$user->id], 'files'=> true, 'method' => 'put']) !!}

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group label-floating">
                                {!! Form::label('name','Name',[ "class"=>"control-label"]) !!}
                                {!! Form::text('name', $user->name,["class"=>"form-control", 'autofocus']) !!}
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group label-floating">
                                {!! Form::label('email','Email Address',[ "class"=>"control-label"]) !!}
                                {!! Form::text('email', $user->email,["class"=>"form-control", 'autofocus']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group label-floating">
                                {!! Form::label('role','Role',[ "class"=>"control-label"]) !!}
                                {!! Form::text('role', $user->role,["class"=>"form-control", 'autofocus']) !!}
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group label-floating">
                                {!! Form::label('image','Role',[ "class"=>"control-label"]) !!}
                                {!! Form::file('image', $user->image,["class"=>"form-control", 'autofocus']) !!}
                            </div>

                        </div>
                    </div>

                    <div class="form-group">

                        {!! Form::submit('Update', ["type"=>"submit", "class" => "btn btn-primary"]) !!}

                    </div>
                {!! Form::close() !!}
        </div>
    </div>
@endsection