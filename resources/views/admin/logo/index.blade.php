@extends('admin.layouts.master')
@section('title','logo')
@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
@endpush
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header " data-background-color="purple" >
                <h4 class="title">Logo
                    <span class="pull-right"><a href="{{url('admin/logo/create')}}" title="Add Logo"><i class="material-icons">note_add</i>Add New</a></span>
                </h4>

            </div>
            <div class="card-content table-responsive">

                <table id="table" class="table table-striped table-bordered">
                    <thead class="text-primary">
                    <tr>
                        <th>Sl no</th>
                        <th>Logo</th>
                        <th>Staus</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($logos as $logo)
                        <tr>

                            <td>{{$logo->id}}</td>
                            <td><img src="{{url('uploads/logo/',$logo->image)}}" style="border-radius:5px; height:80px; width:80px;" alt=""></td>
                            <td>
                                @if($logo->status == true)
                                    <span class="label label-info">Active</span>
                                @else
                                    <span class="label label-danger">Unactive</span>
                                @endif
                            </td>
                            <td>{{$logo->created_at}}</td>
                            <td>{{$logo->updated_at}}</td>

                            <td>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        @if($logo->status == false)
                                            <form id="active-form-{{$logo->id}}" action="{{route('logo.active',$logo->id)}}" style="display: none;" method="post">
                                                @csrf
                                            </form>
                                            <button type="button" class="btn btn-sm btn-warning" onclick="if(confirm('Are you sure logo active?')){
                                                    event.preventDefault();
                                                    document.getElementById('active-form-{{$logo->id}}').submit();
                                                    }else{
                                                    event.preventDefault();
                                                    }"><i class='fa fa-thumbs-down'></i>

                                            </button>
                                        @elseif($logo->status == true)
                                            <form id="unactive-form-{{$logo->id}}" action="{{route('logo.unactive',$logo->id)}}" style="display: none;" method="post">
                                                @csrf
                                            </form>
                                            <button type="button" class="btn btn-sm btn-success" onclick="if(confirm('Are you sure logo unactive?')){
                                                    event.preventDefault();
                                                    document.getElementById('unactive-form-{{$logo->id}}').submit();
                                                    }else{
                                                    event.preventDefault();
                                                    }"><i class='fa fa-thumbs-up'></i>

                                            </button>

                                        @endif
                                    </li>
                                    <li class="list-inline-item"><a class="btn btn-sm btn-info" href="{{url( 'admin/logo/'.$logo->id.'/edit')}}" ><i class="fa fa-pencil"></i></a></li>
                                    <li class="list-inline-item">
                                        {!! Form::open(['url'=>['admin/logo/'.$logo->id],'method'=>'delete']) !!}
                                        {!! Form::button("<i class='fa fa-trash'></i>",[
                                        'type'=>'submit',
                                        'onClick'=>"return confirm('Are You sure Delete $logo->slider ?')",
                                        'class'=>'btn btn-sm btn-danger'
                                        ]) !!}
                                        {!! Form::close() !!}
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script >
        $(document).ready(function() {
            $('#table').DataTable();
        } );
    </script>
@endpush