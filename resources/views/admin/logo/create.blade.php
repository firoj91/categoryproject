@extends('admin.layouts.master')
@section('title','Logo')
@section('content')
    <div class="col-md-12">

        <div class="card">

            <div class="card-header" data-background-color="purple">
                <h4 class="title">Categories
                    <span class="pull-right"><a href="{{url('admin/logo')}}" ><i class="material-icons">list</i>All logo</a></span>
                </h4>
            </div>

            <div class="card-content">

                <div class="row">
                    {!! Form::open(['url' => 'admin/logo/',  'files'=>true]) !!}
                    @include('admin.logo.form')

                    <div class="form-group">
                        {!! Form::submit('Add',["class"=>"btn btn-primary"]) !!}
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection