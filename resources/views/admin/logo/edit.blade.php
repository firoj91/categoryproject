@extends('admin.layouts.master')
@section('title','logo')
@section('content')
    <div class="col-md-12">
        @include('admin.layouts.msg')
        <div class="card">
            <div class="card-header" data-background-color="purple">
                <h4 class="title">Logo
                    <span class="pull-right"><a href="{{url('admin/logo')}}" ><i class="material-icons">list</i>All Logo</a></span>
                </h4>

            </div>

            <div class="card-content">
                <div class="row">
                    {!! Form::model( $logo, ['url' => ['admin/logo', $logo->id], 'files'=> true, 'method' => 'put']) !!}

                    @include('admin.logo.form')

                    <div class="form-group">

                        {!! Form::submit('Update', ["type"=>"submit", "class" => "btn btn-primary"]) !!}

                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection